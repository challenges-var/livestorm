import { HTTP } from './http';

/* eslint-disable */
export const fetchData = (token, date) => new Promise((resolve, reject) => HTTP.get(`https://api.producthunt.com/v1/posts?access_token=${token}&day=${date}`)
  .then(response => ((response.status === 200) ? resolve : reject)(response))
  .catch(error => reject(error.response)));