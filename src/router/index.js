import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Products from '@/views/Products.vue';
import SingleProduct from '@/views/SingleProduct.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/products', name: 'products', component: Products },
    { path: '/products/:id', name: 'single-product', component: SingleProduct },
  ],
});
