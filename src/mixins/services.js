export default {
  methods: {
    /* eslint-disable */
    sumValue(arr) {
      let total = 0;
      for (const i in arr) {
        total += arr[i];
      }
      return total;
    },
    sumLength(arr) {
      let total = 0;
      for (const i in arr) {
        total += i.length;
      }
      return total;
    },
  },
};
