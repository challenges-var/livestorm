import Vue from 'vue';
import Vuex from 'vuex';
import { fetchData } from '@/api';

Vue.use(Vuex);

/* eslint-disable no-console */
export default new Vuex.Store({
  namespaced: true,
  state: {
    data: [],
    token: 'a9e89a2327479d123ea5918fac0a295bc004b8d606f18ebdfe382c98583b402a',
  },
  actions: {
    getPosts({ commit }, date) {
      return new Promise((resolve, reject) => {
        fetchData(this.state.token, date)
          .then((response) => {
            const endpoint = response.data.posts;
            // console.log(endpoint);
            // const posts = endpoint;
            commit('SET_POSTS', endpoint);
            resolve(response);
          })
          .catch((error) => {
            // console.error(error);
            reject(error);
          });
      });
    },
  },
  mutations: {
    SET_POSTS(state, data) {
      Vue.set(state, 'data', data);
    },
  },
});
